using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Email é obrigatório.")]
        [MinLength(10, ErrorMessage = "Email deve ter no mínimo 10 caracteres.")]
        [MaxLength(100, ErrorMessage = "Email deve ter no máximo 100 caracteres.")]
        [EmailAddress(ErrorMessage = "Digite um email válido.")]
        [Display(Name = "E-mail")]
        public string Email{get; set;}

        [Required(ErrorMessage = "Senha é obrigatório.")]
        [MinLength(6, ErrorMessage = "Senha deve ter no mínimo 6 caracteres.")]
        [MaxLength(100, ErrorMessage = "Senha deve ter no máximo 20 caracteres.")]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password{get; set;}

        [Required(ErrorMessage = "Confirmação de Senha é obrigatório.")]
        [MinLength(6, ErrorMessage = "Confirmação de Senha deve ter no mínimo 6 caracteres.")]
        [MaxLength(100, ErrorMessage = "Confirmação de Senha deve ter no máximo 20 caracteres.")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Senhas não são iguais")]
        [Display(Name = "Confirmação de Senha")]
        public string ConfirmPassword{get; set;}
    }
}