using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class TelefoneViewModel
    {
        public int Id {get; set;}
        public int ContatoId {get; set;}

        [Required(ErrorMessage = "O 'Numero' deve ser preenchido")]
        [MinLength(9, ErrorMessage = "O 'Numero' deve ter no minimo 9 caracteres")]
        [MaxLength(100, ErrorMessage = "O 'Nome' deve ter no máximo 100 caracteres")]
        [DisplayName("Numero")]
        public string Numero {get; set;}
    }
}