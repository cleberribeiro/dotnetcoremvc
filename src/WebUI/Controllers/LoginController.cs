using Microsoft.AspNetCore.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Register(){
            ViewData["Title"] = "Registrar";
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel registerViewModel){

            ViewData["Title"] = "Registrar";

            if(ModelState.IsValid) return View();
            
            return RedirectToAction("Register");
        }
    }
}