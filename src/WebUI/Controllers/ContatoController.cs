using System.Collections.Generic;
using AutoMapper;
using DAO;
using DTO;
using Microsoft.AspNetCore.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class ContatoController : Controller
    {

        private IContatoDAO _contatoDAO;
        private IMapper _mapper;
        private ITelefoneDAO _telefoneDAO;

        public ContatoController(IContatoDAO contatoDAO, IMapper mapper, ITelefoneDAO telefoneDAO){
            _contatoDAO = contatoDAO;
            _mapper = mapper;
            _telefoneDAO = telefoneDAO;
        }

        public IActionResult Details(int id){

            var contatoDTO = _contatoDAO.Consultar(id);
            var contatoViewModel = _mapper.Map<ContatoViewModel>(contatoDTO);
            var listTelefoneDTO = _telefoneDAO.ConsultarPorContato(id);
            
            foreach (var telefoneDTO in listTelefoneDTO)
                contatoViewModel.ListTelefoneViewModel.Add(_mapper.Map<TelefoneViewModel>(telefoneDTO));

            return View(contatoViewModel);
        }

        public IActionResult Index(){

            ViewData["Title"] = "Contato";
            ViewData["variavel"] = "teste";

            var contatoDTO = _contatoDAO.Consultar();
            var listContato = new List<ContatoViewModel>();

            foreach (var contato in contatoDTO)
                listContato.Add(_mapper.Map<ContatoViewModel>(contato));

            return View(listContato);
        }

        public IActionResult Create(){

            ViewData["Title"] = "Novo Contato";

            return View();
        }

        [HttpPost]
        public IActionResult Create(ContatoViewModel contatoViewModel)
        {
            if(!ModelState.IsValid) return View();

            var contatoDTO = _mapper.Map<ContatoDTO>(contatoViewModel);
            
            try
            {
                _contatoDAO.Criar(contatoDTO);
                TempData[Constants.Message.SUCCESS] = "Contato criado com sucesso";
            }
            catch (System.Exception e)
            {
                TempData[Constants.Message.ERROR] = e.Message;
            }
            

            return RedirectToAction("Index");
        }

        public IActionResult Update(int Id){
 
            ViewData["Title"] = "Atualizar Contato";

            var contatoDTO = _contatoDAO.Consultar(Id);

            var contato = _mapper.Map<ContatoViewModel>(contatoDTO);

            return View(contato);
        }

        [HttpPost]
        public IActionResult Update(ContatoViewModel contatoViewModel)
        {

            if(!ModelState.IsValid) return View();

            var contatoDTO = _mapper.Map<ContatoDTO>(contatoViewModel);

            try
            {
                _contatoDAO.Atualizar(contatoDTO);
                TempData[Constants.Message.SUCCESS] = "Contato (" + contatoViewModel.Nome + ") atualizado com sucesso!";
            }
            catch (System.Exception e)
            {
                TempData[Constants.Message.ERROR] = "Não foi possível atualizar o contato (" + contatoViewModel.Nome + "). Erro: " + e.Message;
            }
            

            return RedirectToAction("Index");
        }

        public IActionResult Delete(int Id){
            _contatoDAO.Excluir(Id);
            return RedirectToAction("Index");
        }
    }
}