using DAO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace WebUI.StartupConfigure
{
    public static class DependencyInjectionConfigure
    {
        public static IServiceCollection AddDependencyInjection(this IServiceCollection services, IWebHostEnvironment env, IConfiguration configuration){
            if(env.IsDevelopment()){
                services.AddTransient<IContatoDAO, DAO.SQLite.ContatoDAO>();
                services.AddTransient<ITelefoneDAO, DAO.SQLite.TelefoneDAO>();
            }else{
                var conString = configuration.GetConnectionString("SQLServer");
                services.AddTransient<IContatoDAO>(o => new DAO.SQLServer.ContatoDAO(conString));
                services.AddTransient<ITelefoneDAO>(o => new DAO.SQLServer.TelefoneDAO(conString));
            }
            return services;
        }
    }
}