using System;
using System.Data.SqlClient;

namespace DAO.SQLServer
{
    public class BaseDAO
    {
        private readonly string _connectionString;

        public BaseDAO(string connectionString){
            _connectionString = connectionString;
        }

        //private string connectionString = "Data Source=localhost;Initial Catalog=hml;User ID=sa;Password=cleber1@sqlserver;";

        protected SqlConnection Connection => new SqlConnection(_connectionString);

        /*
        public BaseDAO(){
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "localhost";
            builder.UserID = "sa";
            builder.Password = "cleber1@sqlserver";
            builder.InitialCatalog = "hml";
            Connection = new SqlConnection(builder.ToString());
        }
        */
    
    }
}