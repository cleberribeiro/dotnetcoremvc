

using System.Collections.Generic;
using DTO;

namespace DAO
{

    public interface ITelefoneDAO
    {
        void Atualizar(TelefoneDTO telefoneDTO);
        TelefoneDTO Consultar(int Id);
        List<TelefoneDTO> ConsultarPorContato(int ContatoId);
        void Criar(TelefoneDTO telefone);
        void Excluir(int Id);
    }
}