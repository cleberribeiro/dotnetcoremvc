using System.Collections.Generic;
using System.Linq;
using Dapper;
using DTO;

namespace DAO.SQLite
{

    public class TelefoneDAO : BaseDAO, ITelefoneDAO
    {
        public TelefoneDAO() : base()
        {

        }

        public List<TelefoneDTO> ConsultarPorContato(int ContatoId)
        {
            using (var con = Connection)
            {
                con.Open();
                var result = con.Query<TelefoneDTO>(
                    @"SELECT Id, ContatoId, Numero FROM Telefone WHERE ContatoId = @ContatoId;", new { ContatoId = ContatoId }
                ).ToList();
                return result;
            };
        }

        public TelefoneDTO Consultar(int Id)
        {
            using (var con = Connection)
            {
                con.Open();
                var result = con.Query<TelefoneDTO>(
                    @"SELECT Id, ContatoId, Numero FROM Telefone WHERE Id = @Id;", new { Id = Id }
                ).FirstOrDefault();
                return result;
            };
        }

        public void Atualizar(TelefoneDTO telefoneDTO)
        {
            using (var con = Connection)
            {
                con.Execute(
                    @"UPDATE Telefone SET Numero = @Numero WHERE Id = @Id", telefoneDTO
                );
            }
        }

        public void Criar(TelefoneDTO telefone){
            using (var con = Connection)
            {
                con.Open();
                con.Execute(
                    @"INSERT INTO Telefone(
                        Numero, ContatoId
                    ) VALUES (
                        @Numero, @ContatoId
                    )", telefone
                );
            }
        }

        public void Excluir(int Id)
        {
            using (var con = Connection)
            {
                con.Execute(
                    @"DELETE FROM Telefone WHERE Id = @Id", new { Id }
                );
            }
        }
    }
}