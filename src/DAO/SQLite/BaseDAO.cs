using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using Dapper;
using DTO;


namespace DAO.SQLite
{
    public class BaseDAO
    {
    
        protected string DataSourceFile => Environment.CurrentDirectory + "//SisDB.sqlite";
        protected SQLiteConnection Connection => new SQLiteConnection("Data Source=" + DataSourceFile+"; foreing keys=true; PRAGMA foreign_keys = ON;");

        public BaseDAO()
        {
            if (!File.Exists(DataSourceFile)) CreateDatabase();
        }

        public void CreateDatabase()
        {
            using (var con = Connection)
            {
                con.Open();
                con.Execute(
                    @"
                        create table Contato(
                            Id integer primary key autoincrement,
                            Nome varchar(100) not null,
                            Sobrenome varchar(100) not null,
                            Email varchar(100) not null
                        );
                        create table Telefone(
                            Id integer primary key autoincrement,
                            ContatoId integer,
                            Numero varchar(100) not null,
                            FOREIGN KEY (ContatoId) REFERENCES Contato(Id)
                        );
                    "
                );
            }
        }    
    }
}