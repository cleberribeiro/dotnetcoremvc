using System.Collections.Generic;
using System.Data.SQLite;
using DTO;

namespace DAO
{
    public interface IContatoDAO
    {
        void Atualizar(ContatoDTO contato);
        List<ContatoDTO> Consultar();
        ContatoDTO Consultar(int Id);
        void Criar(ContatoDTO contato);
        void Excluir(int Id);
    }
}